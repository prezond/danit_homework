let userName = prompt("Please enter your name", "");
let userAge = +prompt("Enter your age", 0);

let declineUserMessage = "You are not allowed to visit this website.";

while (!isNaN(userName) || userName.length <= 2) {

    userName = prompt("Incorrect name, must contain at least 3 characters including letters.", userName);

}

while (isNaN(userAge) || typeof(userAge) !== "number" || userAge <= 0) {

    userAge = +prompt("Incorrect age, must be positive number.", userAge);

}

if (userAge < 18) {

    alert(declineUserMessage);

} else if (userAge <= 22) {

    let isUserAccept = confirm("Are you sure you want to continue?");

    isUserAccept ? alert(`Welcome, ${userName}`) : alert(declineUserMessage);

} else {

    alert(`Welcome, ${userName}`);

}