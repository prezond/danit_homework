let userFirstNumber = +prompt('Enter a first number', 0);
let userLastNumber = +prompt('Enter a last number', 0);

let count = 0;

while (!Number.isInteger(userFirstNumber) || !Number.isInteger(userLastNumber)) {

    alert("Incorrect numbers, must be integer.");

    userFirstNumber = +prompt('Enter a first number', userFirstNumber);
    userLastNumber = +prompt('Enter a last number', userLastNumber);

}

if (userFirstNumber > userLastNumber) {

    userFirstNumber = userFirstNumber + userLastNumber;
    userLastNumber = userFirstNumber - userLastNumber;
    userFirstNumber = userFirstNumber - userLastNumber;

}

for(let i = userFirstNumber + 1; i < userLastNumber; i++){

    if(i % 5 === 0){
        count++;
        console.log(i);
    } 

}

if (count === 0) {

    alert(`There are no multiples of five between ${userFirstNumber} and ${userLastNumber}`);

}
