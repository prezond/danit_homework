let userNumber = +prompt('Enter a number', 0);

let count = 0;

while (!Number.isInteger(userNumber) || userNumber <= 0) {

    alert("Incorrect number, must be positive integer number.");

    userNumber = +prompt('Enter a number', userNumber);

}

for(let i = 1; i < userNumber; i++){

    if(i % 5 === 0){
        count++;
        console.log(i);
    } 

}

if (count === 0) {

    alert(`There are no multiples of five from 0 to ${userNumber}`);

}