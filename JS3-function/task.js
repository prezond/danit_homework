function calcTwoNumbers (x, y, operator) {

    switch(operator){

        case "+":
            return x + y;

        case "-": 
            return x - y;

        case "*": 
            return x * y;

        case "/":
            if(y === 0) {
                console.warn('Division by zero');
            }

            return x / y;

        default:
            console.error('Unknown operator type');
    }
    
}

const USER_FIRST_NUMBER = +prompt('Enter a first number', 0);
const USER_LAST_NUMBER = +prompt('Enter a second number', 0);

const USER_OPERATOR = prompt('Enter a math operation ( "+" , "-" , "*" , "/" )', "");

console.log(`${USER_FIRST_NUMBER} ${USER_OPERATOR} ${USER_LAST_NUMBER} = ` 
    + calcTwoNumbers(USER_FIRST_NUMBER, USER_LAST_NUMBER, USER_OPERATOR));