function getUserData(msg, validator) { 

    let userData = prompt(msg, "");

    while (!validator(userData)) {

        if (validator(parseInt(userData))) {

            return parseInt(userData);

        }

        if ( userData === null ) {

            return userData;

        }

        userData = prompt("Incorrect, please try again \n\n" + msg, userData);

    }

    return userData;

}

function calcTwoNumbers (x, y, operator) {

    switch(operator){

        case "+":
            return x + y;

        case "-": 
            return x - y;

        case "*": 
            return x * y;

        case "/":
            if(y === 0 || y === null) {
                console.warn('Division by zero');
            }

            return x / y;

        case "%":
            return x % y;

        default:
            console.error('Unknown operator type');
    }
    
}

function isNumber (value) {

    return !isNaN(value) && typeof(value) === "number" 

}

function isMathOperator (operator) {

   return isNaN(operator) && operator.length === 1 && ["+","-","*","/","%"].includes(operator);

}

const USER_FIRST_NUMBER = getUserData("Enter a first number: ", isNumber);
const USER_SECOND_NUMBER = getUserData("Enter a second number: ", isNumber);
const USER_OPERATOR = getUserData("Enter a math operation ( '+' , '-' , '*' , '/' )", isMathOperator);

console.log(`${USER_FIRST_NUMBER} ${USER_OPERATOR} ${USER_SECOND_NUMBER} = ` 
    + calcTwoNumbers(USER_FIRST_NUMBER, USER_SECOND_NUMBER, USER_OPERATOR));
