function createNewUser() {

    let _firstName = prompt('Enter your first name: ');
    let _lastName = prompt('Enter your last name: ');

    const newUser = {

        get firstName() { return _firstName; },
        get lastName() { return _lastName; },

        getLogin: () => (newUser.firstName[0] + newUser.lastName).toLowerCase(),

    };

    Object.defineProperties(newUser , {

        'setFirstName': {
            set(str) {
                _firstName = str;
            },
        },

        'setLastName': {
            set(str) {
                _lastName = str;
            },
        },

    });

    return newUser;

}

const user = createNewUser();

console.log(user.getLogin());
